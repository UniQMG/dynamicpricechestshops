# Dynamic Price Chestshops
An extra sign that changes your [ChestShop](https://www.spigotmc.org/resources/chestshop.51856/) price depending on how much
stuff is available to sell in the chest.

### Sign format
```
DynPrice
%basePrice%
%targetStock%
%margin%
```

#### Base Price
The price per sale when the chest is stocked

#### Target Stock
The target amount of sales to be performed

#### Margin
How much higher the buy price is than the sell/base price

### Usage
Put a DynPrice sign next to a ChestShop one. The Chestshop's
price line will be updated to match the desired price.

### Algorithm
`BuyPrice = basePrice * targetStock / stock * margin`
`SellPrice = basePrice * targetStock / (stock+1)`
