package uniqmg.dynamicpricechestshop;

import com.Acrobot.ChestShop.Listeners.Block.Break.SignBreak;
import com.Acrobot.ChestShop.Signs.ChestShopSign;
import com.Acrobot.ChestShop.Utils.uBlock;
import org.apache.commons.lang.StringEscapeUtils;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.entity.Player;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ConfigSign {
    private final static int[][] sideChecks = {
        {  1,  0,  0 },
        { -1,  0,  0 },
        {  0,  1,  0 },
        {  0, -1,  0 },
        {  0,  0,  1 },
        {  0,  0, -1 }
    };

    public final Sign sign;
    public final Sign chestshopSign;
    public final boolean isConfigSign;
    public final boolean valid;
    public final Exception error;
    public final double basePrice;
    public final int targetStock;
    public final double margin;

    /**
     * Creates a ConfigSign by parsing a sign
     * @param sign the sign to parse
     */
    public ConfigSign(Sign sign) {
        this(sign, null, null);
    }

    /**
     * Creates a ConfigSign by parsing a sign
     * @param sign the sign to parse
     * @param overrideEvent a sign change event that is being fired, overriding the text on the sign
     * @param player optional player who is permission checked against the shop sign
     */
    public ConfigSign(Sign sign, SignChangeEvent overrideEvent, Player player) {
        this.sign = sign;
        Sign chestshopSign = null;
        boolean isConfigSign = false;
        Exception error = null;
        boolean valid = true;
        double basePrice = 0;
        int targetStock = 0;
        double margin = 0;

        String line0 = overrideEvent != null ? overrideEvent.getLine(0) : sign.getLine(0);
        String line1 = overrideEvent != null ? overrideEvent.getLine(1) : sign.getLine(1);
        String line2 = overrideEvent != null ? overrideEvent.getLine(2) : sign.getLine(2);
        String line3 = overrideEvent != null ? overrideEvent.getLine(3) : sign.getLine(3);

        try {
            if (!line0.equals("DynPrice"))
                throw new ConfigSignError("First line must be DynPrice");
            isConfigSign = true;

            try {
                basePrice = Double.parseDouble(line1);
            } catch (NumberFormatException ex) {
                throw new ConfigSignError("Expected double on line 1");
            }
            if (basePrice <= 0) throw new ConfigSignError("Base price must be greater than 0");

            try {
                targetStock = Integer.parseInt(line2);
            } catch (NumberFormatException ex) {
                throw new ConfigSignError("Expected integer on line 2");
            }
            if (targetStock <= 0) throw new ConfigSignError("Target stock must be greater than zero");

            try {
                margin = Double.parseDouble(line3);
            } catch (NumberFormatException ex) {
                throw new ConfigSignError("Expected double on line 3");
            }
            if (margin < 1) throw new ConfigSignError("Margin must be greater than 1");

            chestshopSign = this.findShopSign(player);
        } catch(ConfigSignError ex) {
            valid = false;
            error = ex;
        }

        this.chestshopSign = chestshopSign;
        this.isConfigSign = isConfigSign;
        this.valid = valid;
        this.error = error;
        this.basePrice = basePrice;
        this.targetStock = targetStock;
        this.margin = margin;
    }

    /**
     * Finds the unique shop sign for this config sign
     * @param player an optional player who must be able to edit the chestshop
     * @throws ConfigSignError if not exactly one sign is found
     * @return the chestsign sign
     */
    public Sign findShopSign(Player player) throws ConfigSignError {
        Sign sign = null;

        for (int[] check: sideChecks) {
            Block relblock = this.sign.getBlock().getRelative(check[0], check[1], check[2]);

            BlockData blockData = relblock.getBlockData();
            if (blockData instanceof org.bukkit.block.data.type.Sign || blockData instanceof WallSign) {
                Sign relsign = (Sign) relblock.getState();
                if (ChestShopSign.isValid(relsign)) {
                    if (sign != null)
                        throw new ConfigSignError("Multiple valid chestshop signs");
                    if (player != null && !SignBreak.canBlockBeBroken(relblock, player))
                        throw new ConfigSignError("You don't have permissions for that shop");
                    sign = relsign;
                }
            }
        }

        if (sign == null)
            throw new ConfigSignError("No valid chestshop sign found");
        return sign;
    }

    /**
     * Finds a partially or completely valid (isConfigSign == true) config sign for a given block
     * @param block
     * @return the ConfigSign for the found sign, or null
     */
    public static ConfigSign findConfigSign(Block block) {
        for (int[] check: sideChecks) {
            Block relblock = block.getRelative(check[0], check[1], check[2]);
            System.out.println(String.format("Look %s %s %s: %s", check[0], check[1], check[2], relblock.toString()));

            BlockData blockData = relblock.getBlockData();
            if (blockData instanceof org.bukkit.block.data.type.Sign || blockData instanceof WallSign) {
                ConfigSign sign = new ConfigSign((Sign) relblock.getState());
                if (sign.isConfigSign) return sign;
                System.out.println("Found a sign, but it's not a config sign?? First line: " + StringEscapeUtils.escapeJava(((Sign) relblock.getState()).getLine(0)));
            }
        }

        return null;
    }

    /**
     * Updates the attached chestshopSign's prices to match the dynamic price calculated for this config sign
     * @returns if the sign was changed (false if the new text was the same as the old)
     */
    public boolean updateChestshopSign() {
        Container container = uBlock.findConnectedContainer(this.chestshopSign);
        Inventory inventory = container.getInventory();

        int quantity = Integer.parseInt(this.chestshopSign.getLine(1));

        int items = 0;
        for (ItemStack item: inventory.getStorageContents()) {
            if (item == null) continue;
            items += item.getAmount();
        }
        int transactions = items / quantity;
        int effectiveTransactions = Math.max(transactions, 1);

        double buyprice = Math.max(this.basePrice * (double) this.targetStock / effectiveTransactions, 0.01) * this.margin;
        double sellprice = Math.max(this.basePrice * (double) this.targetStock / (effectiveTransactions+1), 0.01);
        if (transactions == 0) buyprice = 0;

        /*System.out.println(String.format(
            "Dynamic price adjustment: basePrice=%s targetStock=%s quantity=%s transactions=%s buyprice=%s sellprice=%s",
            this.basePrice,
            this.targetStock,
            quantity,
            transactions,
            buyprice,
            sellprice
        ));*/

        String priceLine = String.format("B %.2f : %.2f S", buyprice, sellprice);
        if (this.chestshopSign.getLine(2).equals(priceLine)) return false;
        this.chestshopSign.setLine(2, priceLine);
        this.chestshopSign.update(true);
        return true;
    }
}
