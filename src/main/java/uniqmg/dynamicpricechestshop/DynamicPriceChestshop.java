package uniqmg.dynamicpricechestshop;

import com.Acrobot.ChestShop.Events.PreShopCreationEvent;
import com.Acrobot.ChestShop.Events.PreTransactionEvent;
import com.Acrobot.ChestShop.Events.ShopDestroyedEvent;
import com.Acrobot.ChestShop.Events.TransactionEvent;
import com.Acrobot.ChestShop.Listeners.Block.Break.SignBreak;
import com.Acrobot.ChestShop.Signs.ChestShopSign;
import org.apache.commons.lang.StringEscapeUtils;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;

import static com.Acrobot.ChestShop.Events.PreShopCreationEvent.CreationOutcome.OTHER;

public final class DynamicPriceChestshop extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        // Plugin startup logic
        this.getLogger().info("Supply and Demand, coming to a romco near you.");
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onTransaction(TransactionEvent event) {
        ConfigSign config = ConfigSign.findConfigSign(event.getSign().getBlock());
        if (config == null || !config.valid) return;
        config.updateChestshopSign();
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public static void onPreTransaction(PreTransactionEvent event) {
        ConfigSign config = ConfigSign.findConfigSign(event.getSign().getBlock());
        if (config == null || !config.valid) return;
        if (config.updateChestshopSign()) {
            event.setCancelled(PreTransactionEvent.TransactionOutcome.OTHER);
            event.getClient().sendMessage(String.format(
                "%s%s %s",
                ChatColor.YELLOW,
                "A price update was necessary for this dynamic price shop.",
                "Please re-check the price before purchasing."
            ));
        }

    }

    @EventHandler
    public void onSignChange(SignChangeEvent event) {
        BlockState state = event.getBlock().getState();
        if (!(state instanceof Sign)) return;
        Sign sign = (Sign) state;

        ConfigSign config = new ConfigSign(sign, event, event.getPlayer());
        if (config.isConfigSign && !config.valid) {
            event.getPlayer().sendMessage(String.format(
                "%sDynPrice sign error: %s.",
                ChatColor.RED,
                config.error.getMessage()
            ));
            event.setCancelled(true);
            event.getBlock().breakNaturally();
            return;
        }

        if (config.valid) {
            event.getPlayer().sendMessage(String.format(
                "%sCreated dynamic price shop",
                ChatColor.GREEN
            ));
            config.updateChestshopSign();
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public static void onPreShopCreation(PreShopCreationEvent event) {
        ConfigSign neighborConfig = ConfigSign.findConfigSign(event.getSign().getBlock());
        if (neighborConfig != null) {
            event.setCancelled(true);
            event.setOutcome(OTHER);
            event.getSign().getBlock().breakNaturally();
            event.getPlayer().sendMessage(String.format(
                    "%sThere's a DynPrice sign here already. Break it then place it after your shop sign.",
                    ChatColor.RED
            ));
            return;
        }
    }

    @EventHandler(ignoreCancelled = true)
    public static void onShopDestroy(ShopDestroyedEvent event) {
        ConfigSign neighborConfig = ConfigSign.findConfigSign(event.getSign().getBlock());
        if (neighborConfig != null)
            neighborConfig.sign.getBlock().breakNaturally();
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        BlockData blockData = block.getBlockData();
        if (blockData instanceof org.bukkit.block.data.type.Sign || blockData instanceof WallSign) {
            ConfigSign sign = new ConfigSign((Sign) block.getState());
            if (sign.valid) {
                try {
                    Sign shopSign = sign.findShopSign(null);
                    shopSign.getBlock().breakNaturally();
                    event.getPlayer().sendMessage(String.format(
                        "%sBroke accompanying shop",
                        ChatColor.RED
                    ));
                } catch (ConfigSignError configSignError) {
                }
            }
        }
    }
}
